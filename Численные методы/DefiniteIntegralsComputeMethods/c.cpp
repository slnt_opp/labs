#include <iostream>
#include <iomanip>
#include <math.h>
#include <cmath>

using namespace std;

double f(double x);
double f2(double x, double y);    
double integral_Kubatur_Simpsona(double a,double b,double c, double d,int n,double eps);

int main(){
    double a,b,c,d,eps;
	int n;

	cout<<"Vvedite granici"<<endl;
	cout<<" a = ";
	cin>>a;

	cout<<" b = ";
	cin>>b;
	cout<<endl;

	cout<<"Granici dla 2-go integrala"<<endl;
	cout<<"c = ";
	cin>>c;
	cout<<"d = ";
	cin>>d;
	cout<<endl;

	cout<<"Vvedite razmer pogreshnosti 0.0001 or 0.00001"<<endl;
	cout<<" eps = ";
	cin>>eps;
	cout<<endl;
	
	cout<<"Vvedite kolichestvo razbienii"<<endl;
	cout<<" n = ";
	cin>>n;
	cout<<"=========================================================================="<<endl;
    double	Ik = integral_Kubatur_Simpsona(a,b,c,d,n,eps);
	cout << "integral= "<< Ik << " shagX = "<< ((b-a)/n)<<" shagY = "<< ((d-c)/n)<<endl<<endl;
}

double f2(double x, double y){
    return 4 - x * x - y * y;
}

double integral_Kubatur_Simpsona(double a,double b,double c, double d,int n,double eps)
{
	double I1=0.0, I2=0.0;
	int k = 0;
	while(true)
	{
		int m =n;
		I2=I1;
		double	Hx =(b-a)/(2*n);
		double	Hy =(d-c)/(2*m); 

		double F = 0.0;
		for(int i = 0;i<=n-1;i++)
		{		
			for(int k = 0; k<=m-1; k++)
			{
				F = F+f2(a+Hx*(2*i),c+Hy*(2*k))+4*f2(a+Hx*(2*i+1),c+Hy*(2*k))+
				f2(a+Hx*(2*i+2),c+Hy*(2*k))+4*f2(a+Hx*(2*i),c+Hy*(2*k+1))+
				16*f2(a+Hx*(2*i+1),c+Hy*(2*k+1))+4*f2(a+Hx*(2*i+2),c+Hy*(2*k+1))+
				f2(a+Hx*(2*i),c+Hy*(2*k+2))+4*f2(a+Hx*(2*i+1),c+Hy*(2*k+2))+
				f2(a+Hx*(2*i+2),c+Hy*(2*k+2));
			}
		}
		I1=(Hx*Hy/9)*F;
		n=n*2;

		if(abs(I2-I1)< 3*eps)break;
		k++;
	}
    double I = I1; 
	cout<<"Raznost integralov dI = "<<abs(I1-I2)<<"     k = "<<k<<endl;
	return I;
}